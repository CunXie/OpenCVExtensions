
#include "ArucoMarkerLocator.h"

using namespace std;
using namespace cv;

namespace cve {
    
    void printArucoMarker(int markerFamilyId, int markerId, int size, string filename) {
		Ptr<aruco::Dictionary> dictionary = aruco::getPredefinedDictionary(markerFamilyId);
		Mat out;
		dictionary->drawMarker(markerId, size, out, 1);
		cv::imwrite(filename, out);
	}
    
	void detectArucoMarkersAndPose(const Mat & _imageIn, float markerLength, const cv::Matx33d & _cameraMatrix, const Vec5d & _distCoeffs, vector< int > & _ids, vector< vector< Point2f > > &_corners, vector< Vec3d > & _rvecs, vector< Vec3d > & _tvecs, float minMarkerPerimeterRate, float maxMarkerPerimeterRate, int markerFamilyId){
			Ptr<aruco::DetectorParameters> detectorParams = aruco::DetectorParameters::create();
			detectorParams->doCornerRefinement = true; // do corner refinement in markers
        detectorParams->minMarkerPerimeterRate = minMarkerPerimeterRate;
        detectorParams->maxMarkerPerimeterRate = maxMarkerPerimeterRate;
			Ptr<aruco::Dictionary> dictionary =
				aruco::getPredefinedDictionary(aruco::PREDEFINED_DICTIONARY_NAME(markerFamilyId));
			vector< int > ids;
			vector< vector< Point2f > > corners, rejected;
			vector< Vec3d > rvecs, tvecs;
			// detect markers and estimate pose
			aruco::detectMarkers(_imageIn, dictionary, corners, ids, detectorParams, rejected);
        
			if (ids.size() > 0)
			{
				aruco::estimatePoseSingleMarkers(corners, markerLength, _cameraMatrix, _distCoeffs, rvecs,
					tvecs);
			}
			_ids = ids;
			_corners = corners; 
			_rvecs = rvecs;
			_tvecs = tvecs;
	}
}
