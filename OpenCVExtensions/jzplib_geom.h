//
//  jzplib_geom.h
//  OPENCV_HOTSHOTS
//
//  Created by Zhiping Jiang on 14-7-18.
//
//
#ifndef jzplib_geom_h
#define jzplib_geom_h

#include <opencv2/opencv.hpp>
#include "OpencvMatxExtensions.h"
#include "MatxGeometry.h"

namespace cve {
    
    cv::Mat cropRotatedRectFromImage(const cv::RotatedRect & rotatedRect, const cv::Mat & image);
    cv::Point2f pointInRotatedRect(const cv::RotatedRect & roratedRect, const cv::Point2f & originalPoint);
    cv::Point2f invertPointInRotatedRect(const cv::Point2f & point, const cv::RotatedRect &rotatedRect);

    cv::Matx33d eulerAngles2RotationMatrix(const cv::Vec3d & euler);
    cv::Vec3d rotationMatrix2EulerAngles(const cv::Matx33d & rotationMatrix);
    cv::Vec3d rotationVector2EulerAngles(const cv::Vec3d & rvec);
    
    template<typename _Tp> cv::Vec<_Tp, 2> directionalVector2PitchYaw_CameraCoordinateSystem(cv::Matx<_Tp, 3, 1> dv);
    
    template<typename _Tp> cv::Matx<_Tp, 3, 3> directionalVector2RM_CameraCoordinateSystem(cv::Matx<_Tp, 3, 1> dv);
    
    template<typename _Tp> cv::Matx<_Tp, 3, 3> pitchYaw2RM_CameraCoordinateSystem(cv::Matx<_Tp, 2, 1> dv);
    
    template<typename _Tp> cv::Vec<_Tp, 3> pitchYaw2DirectionalVector_CameraCoordinateSystem(cv::Matx<_Tp, 2, 1> dv);

    template<typename _Tp> _Tp rad2deg(_Tp radian);
    template<typename _Tp> _Tp deg2rad(_Tp degree);
    
    /**
     Calculate the angle (in radian) betweeen two Matx(or Vec) vectors. The dimension and type can be arbitary.

     @return the angle between two vectors.
     */
    template<typename _Tp, int dimension> double angleBetweenTwoVectors(const cv::Matx<_Tp, dimension, 1> & vectorA, const cv::Matx<_Tp, dimension, 1> & vectorB);
    
    /**
     An type-specific extension to the cv::boundingRect. The output Rect type can be specified.

     @param pointsMat 2-chanel m*1 mat containing the points.
     @return a cv::Rect with given type.
     */
    template<typename _Tp>  cv::Rect_<_Tp> boundingRect(const cv::Mat & pointsMat);
    /**
     An type-specific extension to the cv::boundingRect. The output Rect type can be specified.
     
     @param pointsMat 2-chanel m*1 mat containing the points.
     @return a cv::Rect with given type.
     */
    template<typename _Tp>  cv::Rect_<_Tp> boundingRect(const cv::Mat_<_Tp> & pointsMat);
    
    /**
     Calculate the intersection point between a ray and a plane

     @param rayOrigin the ray's origin point
     @param rayDirectionUnitVector normalized ray direction vector
     @param planeOrigin the plane's original point
     @param planeNormal the plane's normal vector (normalized)
     @param forcePositive if true, the intersection point must be in the positive extend of the ray.
     @return the intersection point
     */
    template<typename _Tp> cv::Vec<_Tp, 3> rayPlaneIntersection(const cv::Matx<_Tp, 3, 1> & rayOrigin, const cv::Matx<_Tp, 3, 1> & rayDirectionUnitVector, const cv::Matx<_Tp, 3, 1> & planeOrigin, const cv::Matx<_Tp, 3, 1> & planeNormal, bool forcePositive = true);
    
    /**
     Calcuate the intersection point between a ray and a sphere.
     
     @param rayOrigin ray origin point
     @param rayDirectionUnitVector normalized ray direction vector
     @param sphereOrigin sphere center point
     @param sphereRadius sphere radius
     @return the intersection point
     */
    template<typename _Tp> cv::Vec<_Tp, 3> raySphereIntersection(const cv::Matx<_Tp, 3, 1> & rayOrigin, const cv::Matx<_Tp, 3, 1> & rayDirectionUnitVector, const cv::Matx<_Tp, 3, 1> & sphereOrigin, _Tp sphereRadius);
    
    template<typename _Tp, int dimension> cv::Vec<_Tp, dimension> perpendicularVector2Line(const cv::Matx<_Tp, dimension, 1> & point, const cv::Matx<_Tp, dimension, 1> & lineHead, const cv::Matx<_Tp, dimension, 1> & lineVector);
    
    /**
     Calculate the unit directional vector aligned with the given pixel

     @param cameraMatrix camera trix
     @param pixel the given pixel
     @return the unit directional vector aligned with the given pixel
     */
    template<typename _Tp> cv::Vec<_Tp, 3> pixel2UnitDirectionalVector(const cv::Matx<_Tp, 3, 3> &cameraMatrix, const cv::Matx<_Tp, 2, 1> & pixel);
    
    template<typename _Tp, int dimension> cv::Vec<_Tp, dimension> transformDirectionalVector(const cv::Matx<_Tp, dimension+1, dimension+1> & transform, const cv::Matx<_Tp, dimension, 1> & vector, const cv::Matx<_Tp, dimension, 1> & vectorOrigin = cv::Matx<_Tp, dimension, 1>());
    
    
    template<typename _Tp> std::vector<cv::Point_<_Tp>> projectPointsViaTransform(const std::vector<cv::Point3_<_Tp>> & objectPoints, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion);
    
    template<typename _Tp> cv::Point_<_Tp> projectPointViaTransform(const cv::Point3_<_Tp> & objectPoint, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion);
    
    template<typename _Tp> std::vector<cv::Point_<_Tp>> projectVectorsViaTransform(const std::vector<cv::Vec<_Tp, 3>> & objectPoints, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion);
    
    template<typename _Tp> cv::Point_<_Tp> projectVectorViaTransform(const cv::Vec<_Tp, 3> & objectPoint, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion);
}

#pragma mark - Implementation

namespace cve{
    
    template<typename _Tp> cv::Vec<_Tp, 2> directionalVector2PitchYaw_CameraCoordinateSystem(cv::Matx<_Tp, 3, 1> dv) {
        dv /= cv::norm(dv);
        auto yaw = atan2(dv(0), dv(2));
        auto pitch = asin(dv(1));
        return cv::Vec<_Tp, 2>((_Tp)pitch, (_Tp)yaw);
    }
    
    template<typename _Tp> cv::Matx<_Tp, 3, 3> directionalVector2RM_CameraCoordinateSystem(cv::Matx<_Tp, 3, 1> dv) {
        dv /= cv::norm(dv);
        auto heading = cv::Vec3d(dv(0), 0.0, dv(2));
        auto wingVector = cv::Vec3d(heading(2), 0, -dv(0));
        auto upVector = cv::Vec3d(dv.val).cross(wingVector);
        heading /= cv::norm(heading);
        wingVector /= cv::norm(wingVector);
        upVector /= cv::norm(upVector);
        
        return cv::Matx<_Tp, 3, 3>(wingVector(0), wingVector(1), wingVector(2), upVector(0), upVector(1), upVector(2), dv(0), dv(1), dv(2));
    }
    
    template<typename _Tp> cv::Matx<_Tp, 3, 3> pitchYaw2RM_CameraCoordinateSystem(cv::Matx<_Tp, 2, 1> dv) {
        return cve::matx::rotationMatrixFromEulerAngles(dv(0), dv(1), (_Tp)0.0);
    }
    
    template<typename _Tp> cv::Vec<_Tp, 3> pitchYaw2DirectionalVector_CameraCoordinateSystem(cv::Matx<_Tp, 2, 1> dv) {
        auto rm = pitchYaw2RM_CameraCoordinateSystem(dv);
        return rm * cv::Vec<_Tp,3>(0,0,1);
    }
    
    template<typename _Tp> _Tp rad2deg(_Tp radian) {
        return (_Tp)(1.0 * radian / CV_PI * 180.0);
    }
    
    template<typename _Tp> _Tp deg2rad(_Tp degree) {
        return (_Tp)(1.0 * degree / 180.0 * CV_PI);
    }
    
    template<typename _Tp, int dimension> double angleBetweenTwoVectors(const cv::Matx<_Tp, dimension, 1> & vectorA, const cv::Matx<_Tp, dimension, 1> & vectorB) {
        cv::Vec<_Tp, dimension> normA(vectorA.val);
        cv::Vec<_Tp, dimension> normB(vectorB.val);
        normA /= cv::norm(normA);
        normB /= cv::norm(normB);
        
        return acos(normA.dot(normB));
    }
    
    template<typename _Tp>  cv::Rect_<_Tp> boundingRect(const cv::Mat & pointsMat) {
        _Tp minX, minY, maxX, maxY;
        minX = * (pointsMat.ptr<_Tp>(0) + 0);
        minY = * (pointsMat.ptr<_Tp>(0) + 1);
        maxX = minX;
        maxY = minY;
        for(int i = 0 ; i < pointsMat.rows; i ++) {
            _Tp xValue = * (pointsMat.ptr<_Tp>(i,0) + 0);
            _Tp yValue = * (pointsMat.ptr<_Tp>(i,0) + 1);
            
            minX = (xValue < minX ? xValue : minX);
            minY = (yValue < minY ? yValue : minY);
            maxX = (xValue > maxX ? xValue : maxX);
            maxY = (yValue > maxY ? yValue : maxY);
        }
        
        return cv::Rect_<_Tp>(minX, minY, maxX-minX, maxY-minY);
    }
    
    template<typename _Tp>  cv::Rect_<_Tp> boundingRect(const cv::Mat_<_Tp> & pointsMat) {
        _Tp minX, minY, maxX, maxY;
        minX = * (pointsMat.template ptr<_Tp>(0) + 0);
        minY = * (pointsMat.template ptr<_Tp>(0) + 1);
        maxX = minX;
        maxY = minY;
        for(int i = 0 ; i < pointsMat.rows; i ++) {
            _Tp xValue = * (pointsMat.template ptr<_Tp>(i,0) + 0);
            _Tp yValue = * (pointsMat.template ptr<_Tp>(i,0) + 1);
            
            minX = (xValue < minX ? xValue : minX);
            minY = (yValue < minY ? yValue : minY);
            maxX = (xValue > maxX ? xValue : maxX);
            maxY = (yValue > maxY ? yValue : maxY);
        }
        
        return cv::Rect_<_Tp>(minX, minY, maxX-minX, maxY-minY);
    }
    
    template<typename _Tp> cv::Vec<_Tp, 3> rayPlaneIntersection(const cv::Matx<_Tp, 3, 1> & rayOrigin, const cv::Matx<_Tp, 3, 1> & rayDirectionUnitVector, const cv::Matx<_Tp, 3, 1> & planeOrigin, const cv::Matx<_Tp, 3, 1> & planeNormal, bool forcePositive) {
        
        cv::Matx<_Tp, 3, 1> normalizedVectorDirectionMat = rayDirectionUnitVector * ( 1.0/ cv::norm(rayDirectionUnitVector));
        cv::Matx<_Tp, 3, 1> normalizedPlaneNormalVector = planeNormal * (1.0/ cv::norm(planeNormal));
        _Tp vectorLength = (planeOrigin - rayOrigin).dot(normalizedPlaneNormalVector) /
        normalizedVectorDirectionMat.dot(normalizedPlaneNormalVector);
        
        cv::Matx<_Tp, 3, 1> returnValue;
        if (forcePositive) {
            if (vectorLength > 0) {
                returnValue = rayOrigin + normalizedVectorDirectionMat * vectorLength;
            } else {
                returnValue = cv::Vec<_Tp, 3>();
            }
        } else {
            returnValue = rayOrigin + normalizedVectorDirectionMat * vectorLength;
        }
        
        return cve::vec::fromMatx(returnValue);

    }
    
    template<typename _Tp> cv::Vec<_Tp, 3> raySphereIntersection(const cv::Matx<_Tp, 3, 1> & rayOrigin, const cv::Matx<_Tp, 3, 1> & rayDirectionUnitVector, const cv::Matx<_Tp, 3, 1> & sphereOrigin, _Tp sphereRadius) {
        
        _Tp dx = rayDirectionUnitVector(0);
        _Tp dy = rayDirectionUnitVector(1);
        _Tp dz = rayDirectionUnitVector(2);
        _Tp x0 = rayOrigin(0);
        _Tp y0 = rayOrigin(1);
        _Tp z0 = rayOrigin(2);
        _Tp cx = sphereOrigin(0);
        _Tp cy = sphereOrigin(1);
        _Tp cz = sphereOrigin(2);
        _Tp r = sphereRadius;
        
        _Tp a = dx*dx + dy*dy + dz*dz;
        _Tp b = 2*dx*(x0-cx) + 2*dy*(y0-cy) + 2*dz*(z0-cz);
        _Tp c = cx*cx + cy*cy + cz*cz + x0*x0 + y0*y0 + z0*z0 + -2*(cx*x0 + cy*y0 + cz*z0) - r*r;
        
        _Tp t = (-b - sqrt(b*b - 4*a*c))/2*a;
        
        // This implies that the lines did not intersect, point straight ahead
        if (b*b - 4 * a*c < 0)
            return cv::Vec<_Tp, 3>();
        
        return cv::Vec<_Tp, 3>(x0 + dx * t, y0 + dy * t, z0 + dz * t);
    }
    
    template<typename _Tp, int dimension> cv::Vec<_Tp, dimension> perpendicularVector2Line(const cv::Matx<_Tp, dimension, 1> & point, const cv::Matx<_Tp, dimension, 1> & lineHead, const cv::Matx<_Tp, dimension, 1> & lineVector) {
        auto point3 = cve::vec::changeDimension<3>(point);
        auto lineHead3 = cve::vec::changeDimension<3>(lineHead);
        auto lineVector3 = cve::vec::changeDimension<3>(lineVector);
        lineVector3 /= cv::norm(lineVector3);
        
        auto perpendicularV = lineHead3 - point3 - ((lineHead3 - point3).dot(lineVector3)) * lineVector3;
        return cve::vec::changeDimension<dimension>(perpendicularV);
    }
    
    template<typename _Tp> cv::Vec<_Tp, 3> pixel2UnitDirectionalVector(const cv::Matx<_Tp, 3, 3> &cameraMatrix, const cv::Matx<_Tp, 2, 1> & pixel) {
        
        cv::Vec<_Tp,2> pixelUV = cve::matx::transformVectorWithTransformMat(cameraMatrix.inv(), pixel);
        cv::Vec<_Tp,3> ray = cve::vec::changeDimension<3>(pixelUV, 1.0);
        ray /= cv::norm(ray);
        
        return ray;
    }
    
    template<typename _Tp, int dimension> cv::Vec<_Tp, dimension> transformDirectionalVector(const cv::Matx<_Tp, dimension+1, dimension+1> & transform, const cv::Matx<_Tp, dimension, 1> & vector, const cv::Matx<_Tp, dimension, 1> & vectorOrigin) {
        cv::Vec<_Tp,dimension> vectorHead = cve::matx::transformVectorWithTransformMat(transform, vectorOrigin);
        cv::Vec<_Tp,dimension> vectorEnd = cve::matx::transformVectorWithTransformMat(transform, vector);
        return vectorEnd - vectorHead;
    }
    
    template<typename _Tp> std::vector<cv::Point_<_Tp>> projectPointsViaTransform(const std::vector<cv::Point3_<_Tp>> & objectPoints, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion) {
        std::vector<cv::Point_<_Tp>> outputs;
        cv::Mat tformMat = transform.getMat();
        cv::Mat tvec = tformMat.rowRange(0, 3).colRange(3, 4);
        cv::Mat rm = tformMat.rowRange(0, 3).colRange(0, 3);
        cv::Mat rvec; cv::Rodrigues(rm, rvec);
        cv::projectPoints(objectPoints, rvec, tvec, cameraMatrix, distortion, outputs);
        return outputs;
    }
    
    template<typename _Tp> cv::Point_<_Tp> projectPointViaTransform(const cv::Point3_<_Tp> & objectPoint, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion) {
        return projectPointsViaTransform(std::vector<cv::Point3_<_Tp>> {objectPoint}, transform, cameraMatrix, distortion)[0];
    }
    
    template<typename _Tp> std::vector<cv::Point_<_Tp>> projectVectorsViaTransform(const std::vector<cv::Vec<_Tp, 3>> & objectVectors, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion) {
        std::vector<cv::Point3_<_Tp>> point3s(objectVectors.size());
        for(auto i = 0 ; i < objectVectors.size(); i++) {
            point3s[i] = cve::vec::toPoint3<_Tp>(objectVectors[i]);
        }
        
        return projectPointsViaTransform(point3s, transform, cameraMatrix, distortion);
    }
    
    template<typename _Tp> cv::Point_<_Tp> projectVectorViaTransform(const cv::Vec<_Tp, 3> & objectPoint, cv::InputArray transform, cv::InputArray cameraMatrix, cv::InputArray distortion) {
        return projectVectorsViaTransform(std::vector<cv::Vec<_Tp, 3>> {objectPoint}, transform, cameraMatrix, distortion)[0];
    }

}

#endif
