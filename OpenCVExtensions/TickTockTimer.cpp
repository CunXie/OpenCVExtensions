//
//  jzplib_utilities.cpp
//  OpenCVExtensions
//
//  Created by JiangZhping on 2017/1/30.
//  Copyright © 2017年 JiangZhping. All rights reserved.
//

#include "TickTockTimer.h"

namespace cve {

    std::map<std::string, TickTockTimer *> TickTockTimer::instanceMap;

    TickTockTimer & TickTockTimer::tick() {
        startTime = steady_clock::now();
        return *this;
    }

    TickTockTimer & TickTockTimer::tock() {
        time_point<steady_clock> endTime = steady_clock::now();
        duration<double> elipsed_seconds = endTime - startTime;
        auto duration_double = elipsed_seconds.count();
        durations.push_back(duration_double);

        if (durations.size() > durationLength) {
            durations.pop_front();
        }

        auto averageDuration = [&] () {
            return std::accumulate(durations.begin(), durations.end(), 0.0) / durations.size();
        }();

        std::printf("Timer(%s) %.4fs %.1ffps\n", timerName.c_str(), averageDuration, 1.0/averageDuration);
        return *this;
    }

    TickTockTimer & TickTockTimer::getInstance(const std::string &timerName, int timerLength) {
        if (instanceMap.find(timerName) != instanceMap.end()) {
            return *instanceMap[timerName];
        } else {
            auto newTimer = new TickTockTimer(timerName, timerLength);
            instanceMap[timerName] = newTimer;
            return *instanceMap[timerName];
        }
    }

    TickTockTimer &TickTockTimer::tock_instant() {
        time_point<steady_clock> endTime = steady_clock::now();
        duration<double> elipsed_seconds = endTime - startTime;
        auto duration_double = elipsed_seconds.count();
        std::printf("Instant Timer(%s) %.9fs \n", timerName.c_str(), duration_double);

        return *this;
    }

//    TickTockTimer &TickTockTimer::tickTockLambda(std::function<void(void)> codeBlock) {
//        this->tick();
//        codeBlock();
//        this->tock_instant();
//
//        return *this;
//    }
}
