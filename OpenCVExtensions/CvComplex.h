//
//  CvComplex.h
//  OpenCVExtensions
//
//  Created by 蒋志平 on 2017/12/6.
//

#ifndef CvComplex_h
#define CvComplex_h

#include <iostream>
#include <opencv2/opencv.hpp>

namespace cve{
    namespace complex {
        
        cv::Mat1d magnitude(const cv::Mat2d & csi);
        
        cv::Mat1d phase(const cv::Mat2d & csi);
        
        /**
         * Mimic of Matlab unwrap operation, should produce identical result as Matlab.
         *
         * the original version of code is from: https://www.medphysics.wisc.edu/~ethan/phaseunwrap/
         *
         * @param p data
         * @param N data length
         */
        cv::Mat1d unwrap(const cv::Mat1d & phase);
        
        cv::Mat1d linSpace(double start, double end, double interval);
    }
}

#endif /* CvComplex_h */
