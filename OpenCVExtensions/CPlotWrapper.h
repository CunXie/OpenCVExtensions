//
//  CPlotWrapper.h
//  OpenCVExtensions
//
//  Created by 蒋志平 on 2017/12/6.
//

#ifndef CPlotWrapper_h
#define CPlotWrapper_h

#include "CPlot.h"

class Cv2Plot : public CPlot
{
public:
    template<class T> void plot(vector<T> Y, CvScalar color, char type = '*', bool is_need_lined = true);
    template<class T> void plot(vector< Point_<T> > p, CvScalar color, char type = '*', bool is_need_lined = true);
    Mat figure()
    {
        return cv::cvarrToMat(this->Figure);
    }
};

template<class T>
void Cv2Plot::plot(vector<T> Y, CvScalar color, char type, bool is_need_lined)
{
    T tempX, tempY;
    vector<CvPoint2D64f>data;
    for (int i = 0; i < Y.size(); i++)
    {
        tempX = i;
        tempY = Y[i];
        data.push_back(cvPoint2D64f((double)tempX, (double)tempY));
    }
    this->dataset.push_back(data);
    LineType LT;
    LT.type = type;
    LT.color = color;
    LT.is_need_lined = is_need_lined;
    this->lineTypeSet.push_back(LT);
    this->DrawData(this->Figure);
}

template<class T>
void Cv2Plot::plot(vector< Point_<T> > p, CvScalar color, char type, bool is_need_lined)
{
    T tempX, tempY;
    vector<CvPoint2D64f>data;
    for (int i = 0; i < p.size(); i++)
    {
        tempX = p[i].x;
        tempY = p[i].y;
        data.push_back(cvPoint2D64f((double)tempX, (double)tempY));
    }
    this->dataset.push_back(data);
    LineType LT;
    LT.type = type;
    LT.color = color;
    LT.is_need_lined = is_need_lined;
    this->lineTypeSet.push_back(LT);
    
    this->DrawData(this->Figure);
}

#endif /* CPlotWrapper_h */
