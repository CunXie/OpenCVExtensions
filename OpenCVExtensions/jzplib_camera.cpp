//
//  jzplib_camera.cpp
//  JZP_EYE_TRACKING
//
//  Created by Zhiping Jiang on 14-8-23.
//
//

#include "jzplib_camera.h"

namespace cve { namespace calibration {
    
    std::vector<std::vector<cv::Point3f> > calcBoardCornerPositions(int gridW, int gridH, float squareSize, int imagesCount)
    {
        std::vector<std::vector<cv::Point3f> > objectPoints(imagesCount);
        for (int k = 0 ; k <imagesCount; k++) {
            objectPoints[k] = std::vector<cv::Point3f>(0);
            for( int i = 0; i < gridH; i++ )
                for( int j = 0; j < gridW; j++ )
                    objectPoints[k].push_back(cv::Point3f(float( j*squareSize ), float( i*squareSize ), 0));
        }
        //    objectPoints.resize(imagesCount,objectPoints[0]);
        //    cout<<objectPoints.size()<<" "<<objectPoints[0].size()<<endl;
        return objectPoints;
    }
    
    bool chessboardCameraCalibration(int gridW, int gridH, float gridSize, const std::vector<std::string> & imagePaths, cv::Mat & cameraMatrix, cv::Mat & distCoeffs, bool drawCorners) {
        std::vector<cv::Mat> images;
        for (int i = 0; i <imagePaths.size(); i++) {
            cv::Mat image = cv::imread(imagePaths[i]);
            if (image.cols < image.rows) {
                cv::transpose(image, image);
            }
            images.push_back(image);
        }
        
        cv::Size2i grids(gridW,gridH);
        std::vector<std::vector<cv::Point2f> > imagePoints;
        std::vector<int> usefulImgIndeces;
        for (int i = 0 ; i <images.size() ; i ++) {
            std::vector<cv::Point2f> corners;
            bool found = findChessboardCorners(images[i], grids, corners,cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE+cv::CALIB_CB_FAST_CHECK);
            
            if (found) {
                usefulImgIndeces.push_back(i);
                cv::Mat gray;
                cv::cvtColor(images[i], gray, CV_BGR2GRAY);
                cv::cornerSubPix(gray, corners, cv::Size2i(11, 11), cv::Size2i(-1, -1),
                             cvTermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
                imagePoints.push_back(corners);
                if (drawCorners) {
                    drawChessboardCorners(images[i], grids, cv::Mat(corners), found);
                    imshow("chessboard"+std::to_string(i),images[i]);
                    cv::waitKey(200);
                }
            }
        }
        
        if (usefulImgIndeces.empty()) {
            std::cout<<"no chessboard found."<<std::endl;
            return false;
        }
        
        std::vector<std::vector<cv::Point3f> > objectPoints = calcBoardCornerPositions(gridW,gridH, gridSize, (int)imagePoints.size());
        cv::Size imageSize = images[0].size();
        std::vector<cv::Mat> rvecs, tvecs;
        calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix, distCoeffs, rvecs, tvecs);
        
        return true;
    }

} }
